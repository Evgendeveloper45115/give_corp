<?php

namespace app\controllers;

use app\models\Blogers;
use Yii;
use app\models\Gives;
use app\models\search\GivesSearch;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * GivesController implements the CRUD actions for Gives model.
 */
class GivesController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Gives models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new GivesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Gives model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Gives model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Gives();

        if ($model->load(Yii::$app->request->post())) {
            $model->date_start = date('Y-m-d H:i:s', strtotime($model->date_start));
            $model->date_end = date('Y-m-d H:i:s', strtotime($model->date_end));
            $model->status = Gives::STATUS_NEW;
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            };
        }

        return $this->render('create', [
            'model' => $model,
            'blogers' => $this->getBlogers()
        ]);
    }

    protected function getBlogers()
    {
        return ArrayHelper::map(Blogers::find()->all(), 'id', 'name');
    }

    /**
     * Updates an existing Gives model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $model->date_start = date('Y-m-d H:i:s', strtotime($model->date_start));
            $model->date_end = date('Y-m-d H:i:s', strtotime($model->date_end));
          //  $model->status = Gives::STATUS_NEW;
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
            'blogers' => $this->getBlogers()
        ]);
    }
    public function actionNew($id)
    {
        $model = $this->findModel($id);
        $model->status = Gives::STATUS_NEW;
        $model->save(false);

        return $this->redirect('/gives/index');
    }
    public function actionFinish($id)
    {
        $model = $this->findModel($id);
        $model->status = Gives::STATUS_END;
        $model->save(false);
        return $this->redirect('/gives/index');
    }

    /**
     * Deletes an existing Gives model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Gives model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Gives the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Gives::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
