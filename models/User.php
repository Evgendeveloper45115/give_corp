<?php

namespace app\models;

use app\components\MyUrlManager;
use DeepCopy\f006\A;
use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\VarDumper;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $email
 * @property string $password
 * @property string $first_name
 * @property string $last_name
 * @property string $patronymic
 * @property integer $role
 * @property string $promo
 * @property string $instagram_ak
 * @property string $target
 * @property string $status
 * @property string $phone
 */
class User extends ActiveRecord implements IdentityInterface
{
    const ROLE_USER = 0;
    const ROLE_ADMIN = 1;

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    public $orig_password;
    public $new_password;
    public $password1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        return $scenarios;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'first_name', 'last_name', 'patronymic', 'phone', 'instagram_ak', 'target'], 'required', 'message' => '{attribute} не может быть пустым'],
            [['target', 'instagram_ak', 'status', 'password'], 'safe'],
            [['email'], 'email']
            /*['password1', 'compare',
                'compareAttribute' => 'new_password',
                'on' => self::SCENARIO_CHANGE_PASSWORD_USER,
                'message' => 'Пароли не совпадают'
            ],*/
            //['orig_password', 'myCompare', 'on' => self::SCENARIO_CHANGE_PASSWORD_USER],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'Email',
            'password' => 'Пароль',
            'first_name' => ' Имя',
            'last_name' => 'Фамилия',
            'patronymic' => 'Отчество',
            'status' => 'Статус',
            'target' => 'Цель участия',
        ];
    }

    /**
     * Finds an identity by the given ID.
     * @param string|integer $id the ID to be looked for
     * @return IdentityInterface the identity object that matches the given ID.
     * Null should be returned if such an identity cannot be found
     * or the identity is not in an active state (disabled, deleted, etc.)
     */
    public static function findIdentity($id)
    {
        return User::findOne(['id' => $id]);
    }

    /**
     * Finds an identity by the given token.
     * @param mixed $token the token to be looked for
     * @param mixed $type the type of the token. The value of this parameter depends on the implementation.
     * For example, [[\yii\filters\auth\HttpBearerAuth]] will set this parameter to be `yii\filters\auth\HttpBearerAuth`.
     * @return IdentityInterface the identity object that matches the given token.
     * Null should be returned if such an identity cannot be found
     * or the identity is not in an active state (disabled, deleted, etc.)
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return null;
    }

    /**
     * Returns an ID that can uniquely identify a user identity.
     * @return string|integer an ID that uniquely identifies a user identity.
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns a key that can be used to check the validity of a given identity ID.
     *
     * The key should be unique for each individual user, and should be persistent
     * so that it can be used to check the validity of the user identity.
     *
     * The space of such keys should be big enough to defeat potential identity attacks.
     *
     * This is required if [[User::enableAutoLogin]] is enabled.
     * @return string a key that is used to check the validity of a given identity ID.
     * @see validateAuthKey()
     */
    public function getAuthKey()
    {
        return $this->authKey;

    }

    /**
     * Validates the given auth key.
     *
     * This is required if [[User::enableAutoLogin]] is enabled.
     * @param string $authKey the given auth key
     * @return boolean whether the given auth key is valid.
     * @see getAuthKey()
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    public function myCompare($attribute, $params)
    {
        if (!password_verify($this->$attribute, $this->password) && $this->$attribute != $this->password) {
            $this->addError($attribute, 'Старый пароль не совпадает');
        }
    }

    public function updatePassword($new_password)
    {
        $this->password = $new_password;
        //  $this->save(false);
    }

    public function getUserName()
    {
        if ($this->first_name && $this->last_name) {
            return $this->first_name . ' ' . $this->last_name;
        }
        return $this->email;
    }

    public function getUserFIO()
    {
        return $this->first_name . ' ' . $this->last_name . ' ' . $this->patronymic;
    }

    public static function findByEmail($email)
    {
        return User::findOne(['email' => $email]);
    }

    public function login()
    {
        return Yii::$app->user->login($this);
    }

    public function validatePassword($password)
    {
        return $this->password === $password;
    }

    public function getStatusString()
    {

        $statuses = self::getStatusesArray();

        if (isset($statuses[$this->status])) {

            return $statuses[$this->status];
        }

        return 'Ожидание';
    }

    public static function getStatusesArray()
    {
        return [
            self::STATUS_ACTIVE => 'Одобрен',
            self::STATUS_INACTIVE => 'Не одобрен',
        ];
    }

    public function getLabelCssClassByStatus()
    {
        return $this->status == self::STATUS_ACTIVE ? 'label label-primary' : 'label label-danger';
    }

}
