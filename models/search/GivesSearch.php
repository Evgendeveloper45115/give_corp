<?php

namespace app\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Gives;

/**
 * GivesSearch represents the model behind the search form of `app\models\Gives`.
 */
class GivesSearch extends Gives
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'bloger_id', 'count_sub', 'count_seats', 'cost'], 'integer'],
            [['date_start', 'date_end'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Gives::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'bloger_id' => $this->bloger_id,
            'count_sub' => $this->count_sub,
            'date_start' => $this->date_start,
            'date_end' => $this->date_end,
            'count_seats' => $this->count_seats,
            'cost' => $this->cost,
        ]);

        return $dataProvider;
    }
}
