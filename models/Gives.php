<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "gives".
 *
 * @property int $id
 * @property int $bloger_id
 * @property int $count_sub
 * @property string $date_start
 * @property string $date_end
 * @property int $count_seats
 * @property int $cost
 * @property int $title
 * @property int $is_main
 * @property int $status
 * @property int $plus_subscriber
 * @property string $presents
 * @property string $video_link
 * @property string $subscribes_in
 * @property float $all_cost
 * @property GiveHasUser[] $giveHasUsers
 * @property Blogers $bloger
 */
class Gives extends \yii\db\ActiveRecord
{
    const STATUS_NEW = 1;
    const STATUS_END = 2;

    /**
     * {@inheritdoc}
     */
    public function statuses()
    {
        return [
            self::STATUS_NEW => 'Новый',
            self::STATUS_END => 'Закончился',
        ];
    }

    public static function tableName()
    {
        return 'gives';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['bloger_id', 'count_sub', 'count_seats', 'status', 'plus_subscriber'], 'integer'],
            [['cost', 'all_cost'], 'safe'],
            [['date_start', 'date_end', 'title', 'video_link', 'presents', 'subscribes_in'], 'safe'],
            [['is_main'], 'boolean'],
            [['bloger_id'], 'exist', 'skipOnError' => true, 'targetClass' => Blogers::className(), 'targetAttribute' => ['bloger_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'bloger_id' => 'Блогер',
            'count_sub' => 'Подписчиков пришло',
            'date_start' => 'Дата начала',
            'date_end' => 'Дата окночания',
            'count_seats' => 'К-тво мест',
            'cost' => 'Стоимость подписчика',
            'title' => 'Название',
            'is_star' => 'В топе',
            'video_link' => 'Ссылка на видео',
            'presents' => 'Подарки',
            'subscribes_in' => 'Приход подписчиков',
            'status' => 'Статус',
            'is_main' => 'На главную',
            'plus_subscriber' => 'Нарутка(Это число + реальные подписчики)',
            'all_cost' => 'Полная цена',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGiveHasUsers()
    {
        return $this->hasMany(GiveHasUser::className(), ['give_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBloger()
    {
        return $this->hasOne(Blogers::className(), ['id' => 'bloger_id']);
    }
}
