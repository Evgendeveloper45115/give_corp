<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "give_has_user".
 *
 * @property int $id
 * @property int $user_id
 * @property int $give_id
 * @property int $access
 *
 * @property Gives $give
 * @property Users $user
 */
class GiveHasUser extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'give_has_user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'give_id'], 'required'],
            [['user_id', 'give_id', 'access'], 'integer'],
            [['give_id'], 'exist', 'skipOnError' => true, 'targetClass' => Gives::className(), 'targetAttribute' => ['give_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'give_id' => 'Give ID',
            'access' => 'Access',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGive()
    {
        return $this->hasOne(Gives::className(), ['id' => 'give_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }
}
