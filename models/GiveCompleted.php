<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "give_completed".
 *
 * @property int $id
 * @property int $bloger_id
 * @property string $video
 * @property int $count_sub
 * @property int $cost_pay
 * @property string $presents
 *
 * @property Blogers $bloger
 */
class GiveCompleted extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'give_completed';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['bloger_id'], 'required'],
            [['bloger_id', 'count_sub', 'cost_pay'], 'integer'],
            [['presents'], 'string'],
            [['video'], 'string', 'max' => 255],
            [['bloger_id'], 'exist', 'skipOnError' => true, 'targetClass' => Blogers::className(), 'targetAttribute' => ['bloger_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'bloger_id' => 'Bloger ID',
            'video' => 'Video',
            'count_sub' => 'Count Sub',
            'cost_pay' => 'Cost Pay',
            'presents' => 'Presents',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBloger()
    {
        return $this->hasOne(Blogers::className(), ['id' => 'bloger_id']);
    }
}
