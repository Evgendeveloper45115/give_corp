<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;
use yii\web\View;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class UserAsset extends AssetBundle
{

    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/bootstrap.css',
        'css/owl.carousel.css',
        'css/main.css',
        'css/media.css',
    ];
    public $js = [
        // 'js/jquery.js',
        'js/owl.carousel.min.js',
        'https://www.youtube.com/iframe_api',
        'js/scripts.js?v=0.2',
    ];
    public $jsOptions = [
        'position' => View::POS_END
    ];

    public $depends = [
        // 'yii\web\YiiAsset',
        // 'yii\bootstrap\BootstrapAsset',
    ];
}
