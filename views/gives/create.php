<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Gives */
/* @var $blogers \app\models\Blogers[] */

$this->title = Yii::t('app', 'Создание гива');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Гивы'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gives-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'blogers' => $blogers
    ]) ?>

</div>
