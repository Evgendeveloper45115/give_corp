<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Gives */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Gives'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="gives-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'bloger_id',
            'count_sub',
            'date_start',
            'date_end',
            'count_seats',
            'cost',
        ],
    ]) ?>

    <?php
    $dataProvider = new \yii\data\ActiveDataProvider([
        'query' => $model->getGiveHasUsers(),
        'pagination' => [
            'pageSize' => 20,
        ],
    ]);

    ?>
    <div><h2>Участвуют в данном Гиве</h2></div>
    <?= \yii\grid\GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'user.first_name',
            'user.last_name',
            'user.email',
            'user.instagram_ak',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
