<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\GivesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Гивы');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gives-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Создать гив'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => 'bloger_id',
                'value' => function (\app\models\Gives $model) {
                    return $model->bloger->name;
                }
            ],
            [
                'attribute' => 'status',
                'value' => function (\app\models\Gives $model) {
                    return $model->statuses()[$model->status];
                }
            ],
            'title',
            'video_link',
            'count_sub',
            'presents',
            'subscribes_in',
            'plus_subscriber',
            'date_start',
            'date_end',
            'count_seats',
            'cost',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete} {view} {new} {finish}',
                'buttons' => [
                    'update' => function ($url, $model) {
                        return Html::a('<i class="glyphicon glyphicon-pencil"></i>', \yii\helpers\Url::to(['/gives/update', 'id' => $model->id]), [
                        ]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<i class="glyphicon glyphicon-trash"></i>', \yii\helpers\Url::to(['/gives/delete', 'id' => $model->id]), [
                        ]);
                    },
                    'view' => function ($url, $model) {
                        return Html::a('<i class="glyphicon glyphicon-eye-open"></i>', \yii\helpers\Url::to(['/gives/view', 'id' => $model->id]), [
                        ]);
                    },
                    'new' => function ($url, $model) {
                        return Html::a('<i title="Новый" class="glyphicon glyphicon-ok"></i>', \yii\helpers\Url::to(['/gives/new', 'id' => $model->id]), [
                        ]);
                    },
                    'finish' => function ($url, $model) {
                        return Html::a('<i title="Закончить" class="glyphicon glyphicon-remove"></i>', \yii\helpers\Url::to(['/gives/finish', 'id' => $model->id]), [
                        ]);
                    }
                ]
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
