<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Gives */
/* @var $form yii\widgets\ActiveForm */
/* @var $blogers \app\models\Blogers[] */

?>

<div class="gives-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput() ?>
    <?= $form->field($model, 'bloger_id')->widget(\kartik\select2\Select2::classname(), [
        'data' => $blogers,
        'language' => 'ru',
        'options' => ['placeholder' => 'Выбрать блогера'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'count_sub')->textInput() ?>
    <?= $form->field($model, 'cost')->textInput() ?>
    <?= $form->field($model, 'all_cost')->textInput() ?>
    <?= $form->field($model, 'video_link')->textInput() ?>
    <?= $form->field($model, 'presents')->textInput() ?>
    <?= $form->field($model, 'is_main')->checkbox() ?>

    <?php
    echo '<label>Дата старта</label>';
    echo \kartik\datetime\DateTimePicker::widget([
        'model' => $model,
        'attribute' => 'date_start',
        'options' => ['placeholder' => 'Выбрать дату начала'],
        'convertFormat' => true,
        'pluginOptions' => [
            'format' => 'dd.MM.yyyy HH:i',
            'startDate' => '01-Mar-2019 12:00 AM',
            'todayHighlight' => true
        ]
    ]);
    echo '<label>Дата старта(Конец)</label>';
    echo \kartik\datetime\DateTimePicker::widget([
        'model' => $model,
        'attribute' => 'date_end',
        'options' => ['placeholder' => 'Выбрать дату начала'],
        'convertFormat' => true,
        'pluginOptions' => [
            'format' => 'dd.MM.yyyy HH:i',
            'startDate' => '01-Mar-2019 12:00 AM',
            'todayHighlight' => true
        ]
    ]);
    ?>
    <?= $form->field($model, 'count_seats')->textInput() ?>
    <?= $form->field($model, 'subscribes_in')->textInput() ?>
    <?= $form->field($model, 'plus_subscriber')->textInput() ?>


    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
