<?php
/**
 * @var $give \app\models\Gives
 */
?>
<div class="table_popup table_popup_top table_popup_bloger"><span
            class="table_popup_close"><svg
                viewBox="0 0 16 16">
                                            <path fill-rule="evenodd" fill="rgb(77, 115, 215)"
                                                  d="M15.810,1.103 L8.913,7.999 L15.810,14.896 C16.062,15.149 16.062,15.558 15.810,15.810 C15.684,15.937 15.518,16.000 15.353,16.000 C15.188,16.000 15.022,15.937 14.896,15.810 L7.999,8.913 L1.103,15.810 C0.977,15.937 0.811,16.000 0.646,16.000 C0.480,16.000 0.315,15.937 0.189,15.810 C-0.064,15.558 -0.064,15.149 0.189,14.896 L7.085,7.999 L0.189,1.103 C-0.064,0.850 -0.064,0.441 0.189,0.188 C0.441,-0.064 0.850,-0.064 1.103,0.188 L7.999,7.085 L14.896,0.188 C15.148,-0.064 15.558,-0.064 15.810,0.188 C16.062,0.441 16.062,0.850 15.810,1.103 Z"></path>
                                        </svg></span>
    <div class="table_popup_bloger_photo"
         style="background-image: url(img/<?= $give->bloger->avatar ?>)"></div>
    <div class="table_popup_bloger_name"><?= $give->bloger->name ?></div>
    <a class="table_popup_bloger_inst" href="<?= $give->bloger->instagram ?>"><?= $give->bloger->instagram_name ?></a>
    <div class="table_popup_bloger_sub"><?= $give->bloger->count_subscribes ?>
        подписчиков
    </div>
    <div class="table_popup_bloger_text"><?= $give->bloger->text ?>
    </div>
    <a class="table_popup_bloger_link"
       href="<?= \yii\helpers\Url::to(['/main/gives-completed', 'bloger' => $give->bloger_id]) ?>">Проведенные
        гивы</a>
    <div class="table_popup_bloger_bottom">
        <div class="table_popup_bloger_bottom_next">Следующий
            гив: С <?= date('d-m-Y', strtotime($give->date_start)) ?><br>
            По <?= date('d-m-Y', strtotime($give->date_end)) ?></div>
        <div class="table_popup_bloger_bottom_cost">
                                        <span>Мест: <?= (count($give->giveHasUsers) + $give->plus_subscriber ? $give->plus_subscriber : 0) ?>
                                            /<?= $give->count_seats ?></span><span>Стоимость: <?= $give->all_cost ?>
                руб.</span>
        </div>
        <a class="link"
           href="<?= Yii::$app->user->isGuest ? \yii\helpers\Url::to(['/main/registration']) : \yii\helpers\Url::to(['/main/gives']) ?>">Участвовать
            в гиве</a>
    </div>
</div>
