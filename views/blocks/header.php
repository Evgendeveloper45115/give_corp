<header>
    <div class="wrapper">
        <div class="header"><a class="header_logo logo logo_black" href="/">givecorp</a>
            <div class="header_button"><a class="button button_transparent_blue getconsultation_opener" href="#">
                    Получить консультацию</a></div>
            <div class="header_right"><a class="header_login <?= Yii::$app->user->isGuest ? 'login_opener' : '' ?>"
                                         href="<?= Yii::$app->user->isGuest ? '#' : \yii\helpers\Url::to(['/site/logout']) ?>"><?= Yii::$app->user->isGuest ? 'Войти' : 'Выйти' ?></a><a
                    class="header_menu menu_opener" href="#"><span></span><span
                        class="header_menu_middle"></span><span></span></a></div>
        </div>
    </div>
</header>