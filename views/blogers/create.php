<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Blogers */

$this->title = Yii::t('app', 'Создать блогера');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Блогеры'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="blogers-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
