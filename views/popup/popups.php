<?php

use yii\widgets\ActiveForm;

?>
<div class="getconsultation_popup popup">
    <div class="popup_close">
        <svg viewBox="0 0 16 16">
            <path fill-rule="evenodd" fill="rgb(255, 255, 255)"
                  d="M15.810,1.103 L8.913,7.999 L15.810,14.896 C16.062,15.149 16.062,15.558 15.810,15.810 C15.684,15.937 15.519,16.000 15.353,16.000 C15.188,16.000 15.022,15.937 14.896,15.810 L7.999,8.913 L1.103,15.810 C0.977,15.937 0.811,16.000 0.646,16.000 C0.481,16.000 0.315,15.937 0.189,15.810 C-0.064,15.558 -0.064,15.149 0.189,14.896 L7.085,7.999 L0.189,1.103 C-0.064,0.850 -0.064,0.441 0.189,0.188 C0.441,-0.064 0.850,-0.064 1.103,0.188 L7.999,7.085 L14.896,0.188 C15.148,-0.064 15.558,-0.064 15.810,0.188 C16.062,0.441 16.062,0.850 15.810,1.103 Z"></path>
        </svg>
    </div>
    <div class="getconsultation_center">
        <div class="getconsultation_inner">
            <h2>Получить бесплатную консультацию</h2>
            <div class="getconsultation_text">Нажимая кнопку «Отправить» вы даете согласие на обработку персональных
                данных
            </div>
            <?php $form = ActiveForm::begin([
                'id' => 'consultation',
                'action' => \yii\helpers\Url::to(['/main/consultation']),
                'validateOnChange' => true,
                'validateOnSubmit' => true,
                'enableClientValidation' => true,
                'options' => ['class' => 'getconsultation_form', 'name' => 'sign_in'],
                'fieldConfig' => [
                    'options' => ['class' => ''],
                    'template' => "{label}{input}{error}",
                ],
            ]); ?>
            <div class="getconsultation_form_row">
                <?= $form->field(new \app\models\User(), 'first_name')->textInput(['class' => 'sing_input', 'placeholder' => 'Ваше имя'])->label(false) ?>
                <?= $form->field(new \app\models\User(), 'phone')->textInput(['class' => 'sing_input', 'placeholder' => 'Номер вашего телефона'])->label(false) ?>
            </div>
            <button class="button button_transparent_white">Отправить</button>
            <?php ActiveForm::end(); ?>
            <div class="getconsultation_text_mobile">Нажимая кнопку «Отправить» вы даете согласие на обработку
                персональных данных
            </div>
        </div>
    </div>
</div>
<div class="main_menu">
    <div class="main_menu_inner">
        <div class="main_menu_close">
            <svg viewBox="0 0 16 16">
                <path fill-rule="evenodd" fill="rgb(255, 255, 255)"
                      d="M15.810,1.103 L8.913,7.999 L15.810,14.896 C16.062,15.149 16.062,15.558 15.810,15.810 C15.684,15.937 15.519,16.000 15.353,16.000 C15.188,16.000 15.022,15.937 14.896,15.810 L7.999,8.913 L1.103,15.810 C0.977,15.937 0.811,16.000 0.646,16.000 C0.481,16.000 0.315,15.937 0.189,15.810 C-0.064,15.558 -0.064,15.149 0.189,14.896 L7.085,7.999 L0.189,1.103 C-0.064,0.850 -0.064,0.441 0.189,0.188 C0.441,-0.064 0.850,-0.064 1.103,0.188 L7.999,7.085 L14.896,0.188 C15.148,-0.064 15.558,-0.064 15.810,0.188 C16.062,0.441 16.062,0.850 15.810,1.103 Z"></path>
            </svg>
        </div>
        <div class="main_menu_content">
            <ul>
                <li><a href="#">Участвовать в гиве</a></li>
                <li><a href="#">Блогерам</a></li>
                <li><a href="#">Брендам и магазинам</a></li>
            </ul>
            <div class="main_menu_content_mobile"><a class="<?= Yii::$app->user->isGuest ? 'login_opener' : '' ?>"
                                                     href="<?= Yii::$app->user->isGuest ? '#' : \yii\helpers\Url::to(['/site/logout']) ?>"><?= Yii::$app->user->isGuest ? 'Войти' : 'Выйти' ?></a><a
                        class="getconsultation_opener" onclick="$('.main_menu').hide()">Получить консультацию</a></div>
            <div class="main_menu_write"><a class="button button_transparent_white" href="#">Написать нам в whatsapp</a>
            </div>
            <a class="main_menu_link" href="mailto: support@givecorp.ru">support@givecorp.ru</a>
        </div>
    </div>
</div>
<div class="main_menu_bg"></div>
<div class="login_popup popup">
    <div class="popup_close">
        <svg viewBox="0 0 16 16">
            <path fill-rule="evenodd" fill="rgb(255, 255, 255)"
                  d="M15.810,1.103 L8.913,7.999 L15.810,14.896 C16.062,15.149 16.062,15.558 15.810,15.810 C15.684,15.937 15.519,16.000 15.353,16.000 C15.188,16.000 15.022,15.937 14.896,15.810 L7.999,8.913 L1.103,15.810 C0.977,15.937 0.811,16.000 0.646,16.000 C0.481,16.000 0.315,15.937 0.189,15.810 C-0.064,15.558 -0.064,15.149 0.189,14.896 L7.085,7.999 L0.189,1.103 C-0.064,0.850 -0.064,0.441 0.189,0.188 C0.441,-0.064 0.850,-0.064 1.103,0.188 L7.999,7.085 L14.896,0.188 C15.148,-0.064 15.558,-0.064 15.810,0.188 C16.062,0.441 16.062,0.850 15.810,1.103 Z"></path>
        </svg>
    </div>
    <div class="getconsultation_center">
        <div class="getconsultation_inner">
            <h2>Войти в личный кабинет</h2>
            <div class="getconsultation_text"></div>
            <?php $form = ActiveForm::begin([
                'id' => 'login-form',
                'action' => \yii\helpers\Url::to(['/main/login']),
                'validateOnChange' => true,
                'validateOnSubmit' => true,
                'enableClientValidation' => true,
                'enableAjaxValidation' => true,

                'options' => ['class' => 'getconsultation_form', 'name' => 'sign_in'],
                'fieldConfig' => [
                    'options' => ['class' => ''],
                    'template' => "{label}{input}{error}",
                ],
            ]); ?>
            <div class="getconsultation_form_row">
                <?= $form->field(new \app\models\LoginForm(), 'email')->textInput(['class' => 'sing_input', 'placeholder' => 'Логин (email)'])->label(false) ?>
                <?= $form->field(new \app\models\LoginForm(), 'password')->passwordInput(['class' => 'sing_input', 'placeholder' => 'Пароль'])->label(false) ?>
            </div>
            <button class="button button_transparent_white">Войти</button>
            <?php ActiveForm::end(); ?>
            <a class="getconsultation_text forgot_popup_opener" href="#">Забыли пароль?</a>
            <a class="getconsultation_text_mobile forgot_popup_opener" href="#">Забыли пароль?</a>
        </div>
    </div>
</div>
<div class="video_popup popup">
    <div class="popup_close" id="pausevideo">
        <svg viewBox="0 0 16 16">
            <path fill-rule="evenodd" fill="rgb(255, 255, 255)"
                  d="M15.810,1.103 L8.913,7.999 L15.810,14.896 C16.062,15.149 16.062,15.558 15.810,15.810 C15.684,15.937 15.519,16.000 15.353,16.000 C15.188,16.000 15.022,15.937 14.896,15.810 L7.999,8.913 L1.103,15.810 C0.977,15.937 0.811,16.000 0.646,16.000 C0.481,16.000 0.315,15.937 0.189,15.810 C-0.064,15.558 -0.064,15.149 0.189,14.896 L7.085,7.999 L0.189,1.103 C-0.064,0.850 -0.064,0.441 0.189,0.188 C0.441,-0.064 0.850,-0.064 1.103,0.188 L7.999,7.085 L14.896,0.188 C15.148,-0.064 15.558,-0.064 15.810,0.188 C16.062,0.441 16.062,0.850 15.810,1.103 Z"></path>
        </svg>
    </div>
    <div class="getconsultation_center">
        <div class="getconsultation_inner">
            <div class="video_inner">
                <div class="video_player" id="video_player"></div>
            </div>
        </div>
    </div>
</div>

<div class="sent_popup popup">
    <div class="popup_close">
        <svg viewBox="0 0 16 16">
            <path fill-rule="evenodd" fill="rgb(255, 255, 255)"
                  d="M15.810,1.103 L8.913,7.999 L15.810,14.896 C16.062,15.149 16.062,15.558 15.810,15.810 C15.684,15.937 15.519,16.000 15.353,16.000 C15.188,16.000 15.022,15.937 14.896,15.810 L7.999,8.913 L1.103,15.810 C0.977,15.937 0.811,16.000 0.646,16.000 C0.481,16.000 0.315,15.937 0.189,15.810 C-0.064,15.558 -0.064,15.149 0.189,14.896 L7.085,7.999 L0.189,1.103 C-0.064,0.850 -0.064,0.441 0.189,0.188 C0.441,-0.064 0.850,-0.064 1.103,0.188 L7.999,7.085 L14.896,0.188 C15.148,-0.064 15.558,-0.064 15.810,0.188 C16.062,0.441 16.062,0.850 15.810,1.103 Z"></path>
        </svg>
    </div>
    <div class="getconsultation_center">
        <div class="getconsultation_inner">
            <h2>Заявка на консультацию отправлена!</h2>
            <div class="sent_content">
                <div class="section_signup_successtext">В течение 3-х часов с вами свяжется <br>менеджер для уточнения
                    деталей
                </div>
                <a class="button button_transparent_white" href="#">Продолжить</a>
            </div>
        </div>
    </div>
</div>
<div class="forgot_popup popup">
    <div class="popup_close">
        <svg viewBox="0 0 16 16">
            <path fill-rule="evenodd" fill="rgb(255, 255, 255)"
                  d="M15.810,1.103 L8.913,7.999 L15.810,14.896 C16.062,15.149 16.062,15.558 15.810,15.810 C15.684,15.937 15.519,16.000 15.353,16.000 C15.188,16.000 15.022,15.937 14.896,15.810 L7.999,8.913 L1.103,15.810 C0.977,15.937 0.811,16.000 0.646,16.000 C0.481,16.000 0.315,15.937 0.189,15.810 C-0.064,15.558 -0.064,15.149 0.189,14.896 L7.085,7.999 L0.189,1.103 C-0.064,0.850 -0.064,0.441 0.189,0.188 C0.441,-0.064 0.850,-0.064 1.103,0.188 L7.999,7.085 L14.896,0.188 C15.148,-0.064 15.558,-0.064 15.810,0.188 C16.062,0.441 16.062,0.850 15.810,1.103 Z"></path>
        </svg>
    </div>
    <div class="getconsultation_center">
        <div class="getconsultation_inner">
            <h2>Восстановить пароль</h2>
            <div class="getconsultation_text"></div>
            <?php $form = ActiveForm::begin([
                'id' => 'recover',
                'action' => \yii\helpers\Url::to(['/main/recovery-password']),
                'validateOnChange' => true,
                'validateOnSubmit' => true,
                'enableAjaxValidation' => true,
                'enableClientValidation' => true,
                'options' => ['class' => 'getconsultation_form', 'name' => 'sign_in'],
                'fieldConfig' => [
                    'options' => ['class' => ''],
                    'template' => "{label}{input}{error}",
                ],
            ]); ?>
            <div class="getconsultation_form_row">
                <?= $form->field(new \app\models\User(), 'email')->textInput(['class' => 'tac', 'placeholder' => 'Ваш e-mail'])->label(false) ?>
            </div>
            <button class="button button_transparent_white forgot_popup_sent_opener">Отправить</button>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
<div class="forgot_popup_sent popup">
    <div class="popup_close">
        <svg viewBox="0 0 16 16">
            <path fill-rule="evenodd" fill="rgb(255, 255, 255)"
                  d="M15.810,1.103 L8.913,7.999 L15.810,14.896 C16.062,15.149 16.062,15.558 15.810,15.810 C15.684,15.937 15.519,16.000 15.353,16.000 C15.188,16.000 15.022,15.937 14.896,15.810 L7.999,8.913 L1.103,15.810 C0.977,15.937 0.811,16.000 0.646,16.000 C0.481,16.000 0.315,15.937 0.189,15.810 C-0.064,15.558 -0.064,15.149 0.189,14.896 L7.085,7.999 L0.189,1.103 C-0.064,0.850 -0.064,0.441 0.189,0.188 C0.441,-0.064 0.850,-0.064 1.103,0.188 L7.999,7.085 L14.896,0.188 C15.148,-0.064 15.558,-0.064 15.810,0.188 C16.062,0.441 16.062,0.850 15.810,1.103 Z"></path>
        </svg>
    </div>
    <div class="getconsultation_center">
        <div class="getconsultation_inner">
            <h2>На Ваш e-mail отправлен новый пароль <br>для входа в личный кабинет</h2><a
                    class="button button_transparent_white" href="#">Продолжить</a>
        </div>
    </div>
</div>
