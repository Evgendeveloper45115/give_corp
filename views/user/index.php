<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Новые участники');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?= Html::beginForm(['user/check-status'], 'post'); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Создать участника'), ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('app', 'Новые участники'), \yii\helpers\Url::to(['/user/index']), ['class' => 'btn btn-warning']) ?>
        <?= Html::a(Yii::t('app', 'Одобренные участники'), \yii\helpers\Url::to(['/user/index', 'status' => 1]), ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Отмененные участники'), \yii\helpers\Url::to(['/user/index', 'status' => 0]), ['class' => 'btn btn-danger']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\CheckboxColumn'],
            'first_name',
            'last_name',
            'patronymic',
            'email:email',
            [
                'attribute' => 'status',
                'format' => 'raw',
                'value' => function (\app\models\User $model) {
                    return Html::tag('span', ucfirst($model->getStatusString()), ['class' => $model->getLabelCssClassByStatus()]);
                }
            ],
            //'instagram_ak:ntext',
            'target:ntext',
            //'password',
            //'role',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete} {view} {success} {cancel}',
                'buttons' => [
                    'update' => function ($url, $model) {
                        return Html::a('<i class="glyphicon glyphicon-pencil"></i>', \yii\helpers\Url::to(['/user/update', 'id' => $model->id]), [
                        ]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<i class="glyphicon glyphicon-trash"></i>', \yii\helpers\Url::to(['/user/delete', 'id' => $model->id]), [
                        ]);
                    },
                    'view' => function ($url, $model) {
                        return Html::a('<i class="glyphicon glyphicon-eye-open"></i>', \yii\helpers\Url::to(['/user/view', 'id' => $model->id]), [
                        ]);
                    },
                    'success' => function ($url, $model) {
                        return Html::a('<i title="Одобрить" class="glyphicon glyphicon-ok"></i>', \yii\helpers\Url::to(['/user/success', 'id' => $model->id]), [
                        ]);
                    },
                    'cancel' => function ($url, $model) {
                        return Html::a('<i title="Оменить" class="glyphicon glyphicon-remove"></i>', \yii\helpers\Url::to(['/user/cancel', 'id' => $model->id]), [
                        ]);
                    }
                ]
            ],
        ],
    ]); ?>
    <?= Html::submitButton('Подтвердить', ['class' => 'btn btn-primary', 'name' => 'success']); ?>
    <?= Html::submitButton('Оменить', ['class' => 'btn btn-danger', 'name' => 'cancel']); ?>
    <?= Html::endForm(); ?>
    <?php Pjax::end(); ?>
</div>
