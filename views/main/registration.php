<?php

use yii\widgets\ActiveForm;

/**@var $model \app\models\User */
?>

<section class="section_signup">
    <div class="section_signup_inner">
        <div class="section_signup_header"><a class="logo logo_black" href="index.html">GIVECORP</a><a
                    class="header_menu menu_opener" href="#"><span></span><span
                        class="header_menu_middle"></span><span></span></a></div>
        <div class="section_signup_content">
            <h1>Зарегистрируйтесь в системе, <br>чтобы получить доступ к<br>участию в лучших гивах </h1>
            <?php $form = ActiveForm::begin([
                'id' => 'form-signsup',
                'enableClientValidation' => true,
                'validateOnType' => true,
                'options' => [
                    'class' => 'section_signup_form',
                ],
                'fieldConfig' => [
                    'template' => "<div class='section_signup_form_group section_signup_form_focused'>{label}{input}{error}</div>",
                ],
            ]);
            ?>
            <?= $form->field($model, 'first_name')->textInput()->label('Имя', ['class' => 'section_signup_form_title']) ?>
            <?= $form->field($model, 'last_name')->textInput()->label('Фамилия', ['class' => 'section_signup_form_title']) ?>
            <?= $form->field($model, 'patronymic')->textInput()->label('Отчество', ['class' => 'section_signup_form_title']) ?>
            <?= $form->field($model, 'phone')->textInput()->label('Телефон', ['class' => 'section_signup_form_title']) ?>
            <?= $form->field($model, 'email')->textInput()->label('Email', ['class' => 'section_signup_form_title']) ?>
            <?= $form->field($model, 'instagram_ak')->textInput()->label('Какой аккаунт вы
                        планируете продвигать?', ['class' => 'section_signup_form_title']) ?>
            <?= $form->field($model, 'target')->textInput()->label('Для какой цели вы
                        планируете участвовать в гиве?', ['class' => 'section_signup_form_title']) ?>
            <?= yii\helpers\Html::submitButton('Подать заявку', ['class' => 'button button_transparent_blue']) ?>

            <?php ActiveForm::end(); ?>
        </div>
        <div class="section_signup_footer"><a class="logo logo_grey" href="index.html">GIVECORP</a>
            <div class="copyright">GiveCorp © 2018</div>
            <div class="section_signup_footer_button"><a class="button button_transparent_black getconsultation_opener"
                                                         href="#">Получить консультацию</a></div>
        </div>
    </div>
</section>
<?= Yii::$app->controller->renderPartial('../popup/popups') ?>