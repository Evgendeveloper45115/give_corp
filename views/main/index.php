<?php
/* @var $blogers app\models\Blogers[] */
/* @var $top_give \app\models\Gives */

/* @var $gives \app\models\Gives[] */

use yii\widgets\ActiveForm;

?>


    <section class="section_main">
        <div class="section_main_inner">
            <div class="section_main_l">
                <div class="section_main_l_inner">
                    <div class="section_main_header"><a class="logo logo_black" href="index.html">givecorp</a><a
                                class="section_main_header_menu menu_opener" href="#"><span></span><span
                                    class="section_main_header_menu_middle"></span><span></span></a></div>
                    <div class="section_main_content">
                        <h2>Участвуйте в самых крутых <br>и <span class="colorblue">эффективных </span>гивах в инстаграм
                            с
                            топовыми блогерами</h2>
                        <div class="section_main_l_text">Единственная компания, которая официально организует giveaway
                            по
                            договору
                        </div>
                        <div class="section_main_l_givelist">
                            <div class="section_main_l_givelist_t">от 7 000.-</div>
                            <a class="section_main_l_givelist_link"
                               href="<?= Yii::$app->user->isGuest ? \yii\helpers\Url::to(['/main/registration']) : \yii\helpers\Url::to(['/main/gives']) ?>">Список
                                гивов</a>
                        </div>
                        <div class="section_main_title">Основатели компании популярные инстаблогеры</div>
                        <div class="section_main_founders"><a class="section_main_founder"
                                                              href="https://www.instagram.com/ler_chek" target="_blank">
                                <div class="section_main_founder_photo"
                                     style="background-image: url(img/lerchek.jpg)"></div>
                                <span>@Ler_chek</span>
                            </a><a class="section_main_founder" href="https://www.instagram.com/artem_chek/"
                                   target="_blank">
                                <div class="section_main_founder_photo"
                                     style="background-image: url(img/artemchek.jpg)"></div>
                                <span>@artem_chek</span>
                            </a></div>
                    </div>
                </div>
            </div>
            <div class="section_main_r">
                <div class="section_main_r_inner">
                    <div class="section_main_header"><a class="button button_transparent_white getconsultation_opener"
                                                        href="#">Получить консультацию</a>
                        <div class="section_main_header_r"><a
                                    class="section_main_header_login <?= Yii::$app->user->isGuest ? 'login_opener' : '' ?>"
                                    href="<?= Yii::$app->user->isGuest ? '#' : \yii\helpers\Url::to(['/site/logout']) ?>"><?= Yii::$app->user->isGuest ? 'Войти' : 'Выйти' ?></a><a
                                    class="section_main_header_menu menu_opener"
                                    href="#"><span></span><span
                                        class="section_main_header_menu_middle"></span><span></span></a></div>
                    </div>
                    <div class="section_main_content tac">
                        <div class="section_main_r_title"><span class="section_main_r_title_logo">GIVEAWAY</span><span
                                    class="section_main_r_title-">—</span>Быстрый рост реальных подписчиков
                        </div>
                        <div class="section_main_r_instagrammlogo"></div>
                        <div class="section_main_r_subscribers">
                            <div class="section_main_r_subscribers_quantity">1000000</div>
                            <div class="section_main_r_subscribers_text">
                                <div class="section_main_r_subscribers_text_tip">
                                    <svg viewBox="0 0 15 15">
                                        <path fill-rule="evenodd" fill="rgb(255, 255, 255)"
                                              d="M7.500,7.500 C9.562,7.500 11.250,5.812 11.250,3.749 C11.250,1.687 9.562,-0.000 7.500,-0.000 C5.437,-0.000 3.750,1.687 3.750,3.749 C3.750,5.812 5.437,7.500 7.500,7.500 ZM7.500,9.375 C4.969,9.375 -0.000,10.593 -0.000,13.125 L-0.000,14.999 L15.000,14.999 L15.000,13.125 C15.000,10.593 10.031,9.375 7.500,9.375 Z"></path>
                                    </svg>
                                    1 М
                                </div>
                                <span>подписчиков</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="main_menu"></div>
            </div>
        </div>
    </section>
    <section class="section_advantages">
        <div class="wrapper">
            <h2>Преимущества</h2>
            <div class="advantages">
                <div class="advantage">
                    <div class="advantage_inner">
                        <div class="advantage_icon" style="background-image: url(img/advantage_icon1.png)"></div>
                        <div class="advantage_text"><span class="colorblue">Личная гарантия </span>честности от Леры
                            @Ler_chek и Артема @Artem_chek Чекалиных
                        </div>
                    </div>
                </div>
                <div class="advantage">
                    <div class="advantage_inner">
                        <div class="advantage_icon" style="background-image: url(img/advantage_icon2.png)"></div>
                        <div class="advantage_text"><span class="colorblue">Официальный платеж </span><br>по договору с
                            выдачей электроннго чека
                        </div>
                    </div>
                </div>
                <div class="advantage">
                    <div class="advantage_inner">
                        <div class="advantage_icon" style="background-image: url(img/advantage_icon3.png)"></div>
                        <div class="advantage_text"><span class="colorblue">Более 1500 спонсоров </span>участвовало в
                            гивах,
                            организованных нами
                        </div>
                    </div>
                </div>
                <div class="advantage">
                    <div class="advantage_inner">
                        <div class="advantage_icon" style="background-image: url(img/advantage_icon4.png)"></div>
                        <div class="advantage_text"><span class="colorblue">Более 1 000 000 </span><br>участников <br>розыгрышей
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section_stars">
        <div class="wrapper">
            <h2>Мы сотрудничаем <br>с топовыми блогерами</h2>
            <div class="stars">
                <?php
                foreach ($blogers as $bloger) {
                    $give_blog = \app\models\Gives::find()->where(['bloger_id' => $bloger->id])->limit(1)->orderBy('id DESC')->all();
                    if ($bloger->is_star) {
                        ?>
                        <div class="star">
                            <div class="star_photo" style="background-image: url(img/<?= $bloger->avatar ?>)"></div>
                            <div class="star_name"><?= $bloger->name ?></div>
                            <a class="star_instagramm" href="<?= $bloger->instagram ?>"
                               target="_blank"><?= $bloger->instagram_name ?></a>
                            <div class="star_subscribers"><?= $bloger->count_subscribes ?></div>
                            <a class="star_link"
                               href="<?= Yii::$app->user->isGuest ? \yii\helpers\Url::to(['/main/registration']) : \yii\helpers\Url::to(['/main/gives', 'bloger' => $bloger->id]) ?>"><span>Участвовать в гиве</span></a>
                        </div>
                        <?php
                    }
                }
                ?>

            </div>
            <div class="stars_carousel_buttons">
                <div class="stars_carousel_button stars_carousel_button_prev">
                    <svg viewBox="0 0 30 14">
                        <path d="M0.229,6.429 L6.224,0.236 C6.528,-0.079 7.017,-0.079 7.321,0.236 C7.626,0.551 7.626,1.056 7.321,1.371 L2.653,6.199 L29.220,6.199 C29.651,6.199 30.000,6.560 30.000,7.006 C30.000,7.452 29.651,7.813 29.220,7.813 L2.653,7.813 L7.321,12.629 C7.626,12.944 7.626,13.449 7.321,13.764 C7.169,13.921 6.973,14.000 6.770,14.000 C6.567,14.000 6.370,13.921 6.218,13.764 L0.223,7.564 C-0.075,7.256 -0.075,6.744 0.229,6.429 Z"></path>
                    </svg>
                </div>
                <div class="stars_carousel_button stars_carousel_button_next">
                    <svg viewBox="0 0 30 14">
                        <path d="M29.771,6.429 L23.776,0.236 C23.471,-0.079 22.983,-0.079 22.679,0.236 C22.374,0.551 22.374,1.056 22.679,1.371 L27.347,6.199 L0.780,6.199 C0.349,6.199 -0.000,6.560 -0.000,7.006 C-0.000,7.452 0.349,7.813 0.780,7.813 L27.347,7.813 L22.679,12.629 C22.374,12.944 22.374,13.449 22.679,13.764 C22.831,13.921 23.027,14.000 23.230,14.000 C23.433,14.000 23.630,13.921 23.782,13.764 L29.777,7.564 C30.075,7.256 30.075,6.744 29.771,6.429 Z"></path>
                    </svg>
                </div>
            </div>
        </div>
    </section>
    <section class="section_video">
        <div class="wrapper">
            <div class="section_video_inner">
                <div class="section_video_l">
                    <div class="section_video_l_inner">
                        <div class="gives_item_custom" video="<?= $top_give->video_link ?>"></div>
                        <div class="section_video_video">
                            <div class="section_video_playbutton playvideo">
                                <svg viewBox="0 0 37 44">
                                    <defs>
                                        <lineargradient id="PSgrad_0" x1="0%" x2="42.262%" y1="90.631%" y2="0%">
                                            <stop offset="0%" stop-color="rgb(88,199,219)" stop-opacity="1"></stop>
                                            <stop offset="100%" stop-color="rgb(76,104,214)" stop-opacity="1"></stop>
                                        </lineargradient>
                                    </defs>
                                    <path fill-rule="evenodd" fill="rgb(0, 0, 0)"
                                          d="M34.764,17.929 L7.780,0.767 C6.265,-0.196 4.254,-0.257 2.679,0.607 C1.104,1.470 -0.000,3.058 -0.000,4.851 L-0.000,39.148 C-0.000,40.941 1.104,42.529 2.678,43.392 C3.419,43.798 4.300,44.000 5.099,44.000 C5.998,44.000 6.934,43.743 7.737,43.234 L34.743,26.097 C36.149,25.204 36.999,23.678 37.000,22.013 C37.000,20.350 36.171,18.823 34.764,17.929 L34.764,17.929 ZM33.706,24.439 L6.720,41.576 C5.819,42.148 4.561,42.182 3.626,41.670 C2.690,41.158 1.968,40.216 1.968,39.149 L1.968,4.852 C1.968,3.787 2.690,2.844 3.626,2.332 C4.065,2.091 4.621,1.971 5.095,1.971 C5.629,1.971 6.203,2.123 6.680,2.426 L33.685,19.587 C34.520,20.118 35.028,21.024 35.027,22.013 C35.029,23.002 34.541,23.908 33.706,24.439 Z"></path>
                                    <path fill="url(#PSgrad_0)"
                                          d="M34.764,17.929 L7.780,0.767 C6.265,-0.196 4.254,-0.257 2.679,0.607 C1.104,1.470 -0.000,3.058 -0.000,4.851 L-0.000,39.148 C-0.000,40.941 1.104,42.529 2.678,43.392 C3.419,43.798 4.300,44.000 5.099,44.000 C5.998,44.000 6.934,43.743 7.737,43.234 L34.743,26.097 C36.149,25.204 36.999,23.678 37.000,22.013 C37.000,20.350 36.171,18.823 34.764,17.929 L34.764,17.929 ZM33.706,24.439 L6.720,41.576 C5.819,42.148 4.561,42.182 3.626,41.670 C2.690,41.158 1.968,40.216 1.968,39.149 L1.968,4.852 C1.968,3.787 2.690,2.844 3.626,2.332 C4.065,2.091 4.621,1.971 5.095,1.971 C5.629,1.971 6.203,2.123 6.680,2.426 L33.685,19.587 C34.520,20.118 35.028,21.024 35.027,22.013 C35.029,23.002 34.541,23.908 33.706,24.439 Z"></path>
                                </svg>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
                if ($top_give) {
                    ?>
                    <div class="section_video_r">
                        <div class="section_video_r_inner">
                            <div class="section_video_title">
                                <span>Подписчиков пришло: </span><?= $top_give->count_sub ?>
                            </div>
                            <div class="section_video_title2"><span>Стоимость подписчика: </span><?= $top_give->cost ?>
                                руб.
                            </div>
                            <div class="section_video_text">Подарки: <?= $top_give->presents ?></div>
                            <a class="section_video_link link"
                               href="<?= Yii::$app->user->isGuest ? \yii\helpers\Url::to(['/main/registration']) : \yii\helpers\Url::to(['/main/gives']) ?>">Участвовать
                                в гиве</a>
                        </div>
                    </div>
                    <?php
                }
                ?>
            </div>
        </div>
    </section>
    <section class="section_contract">
        <div class="wrapper">
            <div class="section_contract_inner">
                <div class="section_contract_inner_l">
                    <div class="section_contract_l_inner">
                        <h2>Вы переводите деньги <br>по официальному договору</h2>
                    </div>
                </div>
                <div class="section_contract_inner_r">
                    <div class="section_contract_r_inner">
                        <div class="section_contract_text">Вариант мошенничества исключен полностью</div>
                        <a class="section_contract_link link" href="#">Смотреть договор</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section_givs" id="gives">
        <div class="wrapper">
            <h2>Ближайшие гивы</h2>
            <table class="section_givs_table">
                <thead>
                <tr>
                    <th>Блогер</th>
                    <th class="tac">≈ приход подписчиков</th>
                    <th class="tac">
                        <div class="posrel">
                            <div class="section_givs_withpopups table_popup_date_opener">Примерный старт<span
                                        class="opener"><svg viewbox="0 0 473.733 473.733"
                                                            style="enable-background:new 0 0 473.733 473.733;">
                                            <path class="active-path"
                                                  d="M231.2,336.033c-9.35,0-17,7.65-17,17v11.333c0,9.35,7.65,17,17,17s17-7.65,17-17v-11.333    C248.2,343.683,240.55,336.033,231.2,336.033z"
                                                  data-original="#000000" data-old_color="#4d73d7"
                                                  fill="#4d73d7"></path>
                                            <path class="active-path"
                                                  d="M236.867,473.733c130.617,0,236.867-106.25,236.867-236.867S367.483,0,236.867,0S0,106.25,0,236.867    S106.25,473.733,236.867,473.733z M236.867,34c111.917,0,202.867,90.95,202.867,202.867s-90.95,202.867-202.867,202.867    S34,348.783,34,236.867S124.95,34,236.867,34z"
                                                  data-original="#000000" data-old_color="#4d73d7"
                                                  fill="#4d73d7"></path>
                                            <path class="active-path"
                                                  d="M163.2,194.367C163.483,194.367,163.483,194.367,163.2,194.367c9.35,0,17-7.083,17-16.433c0,0,0.283-13.6,7.083-26.917    c8.5-17,23.517-25.5,45.617-25.5c20.683,0,35.983,5.667,44.483,16.717c7.083,9.067,9.067,21.533,5.667,35.133    c-4.25,16.717-18.7,31.167-32.583,45.333c-17,17.567-34.85,35.417-34.85,59.5c0,9.35,7.65,17,17,17s17-7.65,17-17    c0-10.2,12.183-22.667,25.217-35.7c16.15-16.433,34.567-35.133,41.083-60.633c6.233-23.517,1.983-47.033-11.617-64.317    c-10.483-13.6-31.45-30.033-71.117-30.033c-44.483,0-65.733,23.8-75.933,44.2c-10.2,20.4-10.767,39.95-10.767,42.217    C146.483,187,153.85,194.367,163.2,194.367z"
                                                  data-original="#000000" data-old_color="#4d73d7"
                                                  fill="#4d73d7"></path>
                                        </svg></span></div>
                            <div class="table_popup table_popup_date"><span class="table_popup_close"><svg
                                            viewBox="0 0 16 16">
                                            <path fill-rule="evenodd" fill="rgb(77, 115, 215)"
                                                  d="M15.810,1.103 L8.913,7.999 L15.810,14.896 C16.062,15.149 16.062,15.558 15.810,15.810 C15.684,15.937 15.518,16.000 15.353,16.000 C15.188,16.000 15.022,15.937 14.896,15.810 L7.999,8.913 L1.103,15.810 C0.977,15.937 0.811,16.000 0.646,16.000 C0.480,16.000 0.315,15.937 0.189,15.810 C-0.064,15.558 -0.064,15.149 0.189,14.896 L7.085,7.999 L0.189,1.103 C-0.064,0.850 -0.064,0.441 0.189,0.188 C0.441,-0.064 0.850,-0.064 1.103,0.188 L7.999,7.085 L14.896,0.188 C15.148,-0.064 15.558,-0.064 15.810,0.188 C16.062,0.441 16.062,0.850 15.810,1.103 Z"></path>
                                        </svg></span>
                                <div class="table_popup_title">Дата старта</div>
                                <div class="table_popup_text">Это приблизительная дата старта. Дата может быть
                                    скорректирована на более поздний или более ранний срок.
                                </div>
                                <div class="table_popup_link link">Список гивов</div>
                            </div>
                        </div>
                    </th>
                    <th class="tac">Мест</th>
                    <th>Стоимость</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach ($gives as $give) {
                    ?>
                    <tr>
                        <td class="mobile100">
                            <div class="posrel">
                                <div class="section_givs_bloger table_popup_bloger_opener">
                                    <div class="section_givs_bloger_photo"
                                         style="background-image: url(img/<?= $give->bloger->avatar ?>)"></div>
                                    <div class="section_givs_bloger_instagramm"><?= $give->bloger->instagram_name ?></div>
                                    <div class="section_givs_table_text">Проведенные гивы</div>
                                </div>
                                <?= Yii::$app->controller->renderPartial('../blocks/table_popup', ['give' => $give]) ?>
                            </div>
                        </td>
                        <td class="tac">
                            <div class="posrel">
                                <div class="mobile_th"> ≈ приход подписчиков</div>
                                <div class="table_popup_warranty_opener">
                                    <div class="section_givs_table_title"><?= $give->subscribes_in ?></div>
                                    <div class="section_givs_table_text">Гарантия</div>
                                </div>
                                <div class="table_popup table_popup_top table_popup_warranty"><span
                                            class="table_popup_close"><svg
                                                viewBox="0 0 16 16">
                                            <path fill-rule="evenodd" fill="rgb(77, 115, 215)"
                                                  d="M15.810,1.103 L8.913,7.999 L15.810,14.896 C16.062,15.149 16.062,15.558 15.810,15.810 C15.684,15.937 15.518,16.000 15.353,16.000 C15.188,16.000 15.022,15.937 14.896,15.810 L7.999,8.913 L1.103,15.810 C0.977,15.937 0.811,16.000 0.646,16.000 C0.480,16.000 0.315,15.937 0.189,15.810 C-0.064,15.558 -0.064,15.149 0.189,14.896 L7.085,7.999 L0.189,1.103 C-0.064,0.850 -0.064,0.441 0.189,0.188 C0.441,-0.064 0.850,-0.064 1.103,0.188 L7.999,7.085 L14.896,0.188 C15.148,-0.064 15.558,-0.064 15.810,0.188 C16.062,0.441 16.062,0.850 15.810,1.103 Z"></path>
                                        </svg></span>
                                    <div class="table_popup_title">Гарантия</div>
                                    <div class="table_popup_text">Гарантия — это моральная или материальная
                                        ответственность,
                                        которую принимают на себя частное лицо, государство, предприятие, коммерческая
                                        или
                                        иная
                                        структура за выполнение, соблюдение и т. п. каких-либо обязательств, а также за
                                        состояние, качество чего-либо.
                                    </div>
                                    <a class="table_popup_file" href="#">
                                        <div class="table_popup_file_icon">
                                            <svg viewBox="0 0 28 15">
                                                <path fill-rule="evenodd" fill="rgb(94, 91, 234)"
                                                      d="M24.348,15.000 L3.652,15.000 C1.583,15.000 -0.000,13.500 -0.000,11.538 L-0.000,3.462 C-0.000,1.500 1.583,-0.000 3.652,-0.000 L24.348,-0.000 C26.417,-0.000 28.000,1.500 28.000,3.462 L28.000,11.538 C28.000,13.500 26.417,15.000 24.348,15.000 ZM26.783,3.462 C26.783,2.192 25.687,1.154 24.348,1.154 L3.652,1.154 C2.313,1.154 1.217,2.192 1.217,3.462 L1.217,11.538 C1.217,12.808 2.313,13.846 3.652,13.846 L24.348,13.846 C25.687,13.846 26.783,12.808 26.783,11.538 L26.783,3.462 ZM19.478,6.923 L23.130,6.923 L23.130,8.077 L19.478,8.077 L19.478,11.538 L18.261,11.538 L18.261,3.462 L24.348,3.462 L24.348,4.615 L19.478,4.615 L19.478,6.923 ZM14.609,11.538 L10.956,11.538 L10.956,3.462 L14.609,3.462 C15.948,3.462 17.043,4.500 17.043,5.769 L17.043,9.231 C17.043,10.500 15.948,11.538 14.609,11.538 ZM15.826,5.769 C15.826,5.077 15.339,4.615 14.609,4.615 L12.174,4.615 L12.174,10.385 L14.609,10.385 C15.339,10.385 15.826,9.923 15.826,9.231 L15.826,5.769 ZM7.304,8.077 L4.870,8.077 L4.870,11.538 L3.652,11.538 L3.652,5.769 L3.652,3.462 L7.304,3.462 C8.643,3.462 9.739,4.500 9.739,5.769 C9.739,7.038 8.643,8.077 7.304,8.077 ZM7.304,4.615 L4.870,4.615 L4.870,6.923 L7.304,6.923 C8.035,6.923 8.522,6.461 8.522,5.769 C8.522,5.077 8.035,4.615 7.304,4.615 Z"></path>
                                            </svg>
                                        </div>
                                        <span>"Договор"</span>
                                    </a>
                                    <div class="table_popup_link link">Список гивов</div>
                                </div>
                            </div>
                        </td>
                        <td class="tac">
                            <div class="table_popup_date_opener2">
                                <div class="mobile_th section_givs_withpopups"><span class="text">Примерный старт<span
                                                class="opener"><svg viewbox="0 0 473.733 473.733"
                                                                    style="enable-background:new 0 0 473.733 473.733;">
                                                <path class="active-path"
                                                      d="M231.2,336.033c-9.35,0-17,7.65-17,17v11.333c0,9.35,7.65,17,17,17s17-7.65,17-17v-11.333    C248.2,343.683,240.55,336.033,231.2,336.033z"
                                                      data-original="#000000" data-old_color="#4d73d7"
                                                      fill="#4d73d7"></path>
                                                <path class="active-path"
                                                      d="M236.867,473.733c130.617,0,236.867-106.25,236.867-236.867S367.483,0,236.867,0S0,106.25,0,236.867    S106.25,473.733,236.867,473.733z M236.867,34c111.917,0,202.867,90.95,202.867,202.867s-90.95,202.867-202.867,202.867    S34,348.783,34,236.867S124.95,34,236.867,34z"
                                                      data-original="#000000" data-old_color="#4d73d7"
                                                      fill="#4d73d7"></path>
                                                <path class="active-path"
                                                      d="M163.2,194.367C163.483,194.367,163.483,194.367,163.2,194.367c9.35,0,17-7.083,17-16.433c0,0,0.283-13.6,7.083-26.917    c8.5-17,23.517-25.5,45.617-25.5c20.683,0,35.983,5.667,44.483,16.717c7.083,9.067,9.067,21.533,5.667,35.133    c-4.25,16.717-18.7,31.167-32.583,45.333c-17,17.567-34.85,35.417-34.85,59.5c0,9.35,7.65,17,17,17s17-7.65,17-17    c0-10.2,12.183-22.667,25.217-35.7c16.15-16.433,34.567-35.133,41.083-60.633c6.233-23.517,1.983-47.033-11.617-64.317    c-10.483-13.6-31.45-30.033-71.117-30.033c-44.483,0-65.733,23.8-75.933,44.2c-10.2,20.4-10.767,39.95-10.767,42.217    C146.483,187,153.85,194.367,163.2,194.367z"
                                                      data-original="#000000" data-old_color="#4d73d7"
                                                      fill="#4d73d7"></path>
                                            </svg></span></span></div>
                                <div class="section_givs_table_title">
                                    С <?= date('d-m-Y', strtotime($give->date_start)) ?>
                                    <br>
                                    По <?= date('d-m-Y', strtotime($give->date_end)) ?></div>
                            </div>
                            <div class="table_popup table_popup_date2"><span class="table_popup_close"><svg
                                            viewBox="0 0 16 16">
                                        <path fill-rule="evenodd" fill="rgb(77, 115, 215)"
                                              d="M15.810,1.103 L8.913,7.999 L15.810,14.896 C16.062,15.149 16.062,15.558 15.810,15.810 C15.684,15.937 15.518,16.000 15.353,16.000 C15.188,16.000 15.022,15.937 14.896,15.810 L7.999,8.913 L1.103,15.810 C0.977,15.937 0.811,16.000 0.646,16.000 C0.480,16.000 0.315,15.937 0.189,15.810 C-0.064,15.558 -0.064,15.149 0.189,14.896 L7.085,7.999 L0.189,1.103 C-0.064,0.850 -0.064,0.441 0.189,0.188 C0.441,-0.064 0.850,-0.064 1.103,0.188 L7.999,7.085 L14.896,0.188 C15.148,-0.064 15.558,-0.064 15.810,0.188 C16.062,0.441 16.062,0.850 15.810,1.103 Z"></path>
                                    </svg></span>
                                <div class="table_popup_title">Дата старта</div>
                                <div class="table_popup_text">Это приблизительная дата старта. Дата может быть
                                    скорректирована на более поздний или более ранний срок.
                                </div>
                                <div class="table_popup_link link">Список гивов</div>
                            </div>
                        </td>
                        <td class="tac">
                            <div class="mobile_th"> Мест</div>
                            <div class="section_givs_table_title"> <?= (count($give->giveHasUsers) + $give->plus_subscriber ? $give->plus_subscriber : 0) ?>
                                /<?= $give->count_seats ?></div>
                        </td>
                        <td class="mobile100">
                            <div class="section_givs_table_cost">
                                <a class="section_givs_cost_getprice"
                                   href="<?= \yii\helpers\Url::to(Yii::$app->user->isGuest ? 'main/registration' : '#') ?>"><?= Yii::$app->user->isGuest ? 'Узнать цену' : $give->all_cost ?></a>
                                <a class="link"
                                   href="<?= \yii\helpers\Url::to(Yii::$app->user->isGuest ? 'main/registration' : \yii\helpers\Url::to(['/main/gives'])) ?>">Участвовать</a>
                            </div>
                        </td>
                    </tr>

                    <?php
                }
                ?>
                </tbody>
            </table>
        </div>
    </section>
    <footer class="main_page_footer">
        <div class="wrapper">
            <div class="main_page_footer_inner"><a class="logo logo_grey" href="index.html">givecorp</a>
                <div class="copyright">GiveCorp © 2018</div>
                <div class="main_page_footer_button"><span
                            class="button button_transparent_black getconsultation_opener">Получить консультацию</span>
                </div>
            </div>
        </div>
    </footer>
<?= Yii::$app->controller->renderPartial('../popup/popups') ?>