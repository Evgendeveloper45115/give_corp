<?php
/**
 * @var $this \yii\web\View
 */
?>
    <section class="section_signup">
        <div class="section_signup_inner">
            <div class="section_signup_header"><a class="logo logo_black" href="index.html">GIVECORP</a><a
                        class="header_menu menu_opener" href="#"><span></span><span
                            class="header_menu_middle"></span><span></span></a></div>
            <div class="section_signup_content">
                <h1>Вы успешно подали заявку</h1>
                <div class="section_signup_form">
                    <div class="section_signup_successtext tac">
                        <p>Вашу заявку рассматривает менеджер.</p>
                        <p>После одобрения заявки на указанный email будет выслан логин и пароль для входа в личный
                            кабинет.</p>
                    </div>
                    <div class="section_signup_form_button"><a class="button button_transparent_blue" href="/">Перейти
                            на главную</a>
                    </div>
                </div>
            </div>
            <div class="section_signup_footer"><a class="logo logo_grey" href="/">GIVECORP</a>
                <div class="copyright">GiveCorp © 2018</div>
                <div class="section_signup_footer_button"><a
                            class="button button_transparent_black getconsultation_opener" href="#">Получить
                        консультацию</a></div>
            </div>
        </div>
    </section>
<?= Yii::$app->controller->renderPartial('../popup/popups') ?>