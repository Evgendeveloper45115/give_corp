/* eslint prefer-arrow-callback: 0 */

$(document).ready(function () {
    $('.next_screen').on('click', function () {

    });

    $('.stars_carousel_button_prev').on('click', function () {
        starscarousel.trigger('prev.owl.carousel');
    });
    $('.stars_carousel_button_next').on('click', function () {
        starscarousel.trigger('next.owl.carousel');
    });

    $('.popup_close, .video_popup_bg').on('click', function () {
        $('.popup, .video_popup_bg').fadeOut();
        $('body').removeClass('fixed');
    });

    $('.getconsultation_opener').on('click', function () {
        $('.getconsultation_popup').fadeIn()
        $('body').addClass('fixed');
    });

    $('.section_signup_form_group').on('click', function () {
        $('.section_signup_form_group').each(function () {
            if ($(this).find('input').val().length === 0) {
                // $(this).removeClass('section_signup_form_focused');
            }
        });
        $(this).find('input').focus();
        $(this).addClass('section_signup_form_focused');
    });

    $('.table_popup_date_opener').on('click', function () {
        $('.table_popup').fadeOut();
        $('.table_popup_date').fadeIn()
    });

    $('.table_popup_date_opener2').on('click', function () {
        $('.table_popup').fadeOut();
        $('.table_popup_date2').fadeIn()
    });

    $('.table_popup_warranty_opener').on('click', function () {
        $('.table_popup').fadeOut();
        $(this).closest('.posrel').find('.table_popup_warranty').fadeIn();
    });

    $('.table_popup_bloger_opener').on('click', function () {
        $('.table_popup').fadeOut();
        $(this).closest('.posrel').find('.table_popup_bloger').fadeIn();
        if ($(this).closest('.posrel').find(".table_popup_bloger_photo").offset().top < 0) {
            $(this).closest('.posrel').find(".table_popup").removeClass("table_popup_top");
        }
    });


    $('.table_popup_close').on('click', function () {
        $('.table_popup').fadeOut()
    });

    $('.main_menu_close, .main_menu_bg').on('click', function () {
        $('.main_menu, .main_menu_bg').fadeOut()
        $('body').removeClass('fixed');
    });

    $('.menu_opener').on('click', function () {
        $('.main_menu, .main_menu_bg').fadeIn()
        $('body').addClass('fixed');
    });

    $('.login_opener').on('click', function () {
        $('.login_popup').fadeIn()
        $('body').addClass('fixed');
    });

    $('.sent_popup_opener').on('click', function () {
        $('.sent_popup').fadeIn()
        $('body').addClass('fixed');
    });

    $('.forgot_popup_opener').on('click', function () {
        $('.forgot_popup').fadeIn()
        $('body').addClass('fixed');
    });

    $('.forgot_popup_sent_opener').on('click', function () {
        $('.forgot_popup_sent').fadeIn();
        $('body').addClass('fixed');
    });
    if ($('.section_main_r_subscribers_quantity').length > 0) {
        setInterval(function () {
            $(".section_main_r_subscribers_quantity").text(parseInt($(".section_main_r_subscribers_quantity").text(), 10) + 1)
        }, 300)
    }
    ;
});


var starscarousel = $('.stars');
$(window).on("load resize", function () {
    if ($(window).width() < '1024') {
        starscarousel.removeClass("owl-carousel");
        starscarousel.trigger('destroy.owl.carousel');
        $('.stars_carousel_buttons').addClass("hidden");
    } else {
        starscarousel.owlCarousel({
            nav: true,
            loop: true,
            dots: false,
            items: 4,
            navText: "",
        });
        starscarousel.addClass("owl-carousel");
        $('.stars_carousel_buttons').removeClass("hidden");
    }
});

var player;
var link;
$('.playvideo').on('click', function () {
    link = $(this).closest('.gives_item').attr('video');
    if (!link) {
        link = $('.gives_item_custom').attr('video')
    }
    $('.video_popup, .video_popup_bg').fadeIn();
    player = new YT.Player('video_player', {
        videoId: link,
        playerVars: {
            showinfo: '0',
            rel: '0'
        }
    });
    //   player.playVideo();
});

/*function onYouTubeIframeAPIReady() {
    console.log($('#video_player').attr('link'));
    player = new YT.Player('video_player', {
        videoId: $('#video_player').attr('link'),
        playerVars: {
            showinfo: '0',
            rel: '0'
        }
    });
}*/


$('#pausevideo').on('click', function () {
    $('.video_inner iframe').remove();
    $('.video_inner').append('<div class="video_player" id="video_player"</div>');
    $('.video_popup').fadeOut();
});

$('#fio').on('input', validateFIO);

function validateFIO(e) {
    var $item = $(this),
        value = $item.val();
    var inputFIO = new RegExp('[^a-zA-Zа-яА-Я]+');
    if (!inputFIO.test(value)) {
        $(this).closest(".section_signup_form_group").removeClass("section_signup_form_alert")
        return true;
    } else {
        e.preventDefault();
        $(this).closest(".section_signup_form_group").addClass("section_signup_form_alert")
    }
}

$('#phone').on('input', validatePhone);

function validatePhone(e) {
    var $item = $(this),
        value = $item.val();
    var inputPhone = new RegExp('[^0-9]+');
    if (!inputPhone.test(value)) {
        $(this).closest(".section_signup_form_group").removeClass("section_signup_form_alert")
        return true;
    } else {
        e.preventDefault();
        $(this).closest(".section_signup_form_group").addClass("section_signup_form_alert")
    }
}


